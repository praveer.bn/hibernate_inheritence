package com;


public class PartTimeEmp extends Employee {
    private int pNumberOfHours;
    private int pSalaryPerHour;

    public int getpNumberOfHours() {
        return pNumberOfHours;
    }

    public void setpNumberOfHours(int pNumberOfHours) {
        this.pNumberOfHours = pNumberOfHours;
    }

    public int getpSalaryPerHour() {
        return pSalaryPerHour;
    }

    public void setpSalaryPerHour(int pSalaryPerHour) {
        this.pSalaryPerHour = pSalaryPerHour;
    }

    public PartTimeEmp(int empId, String empName, int pNumberOfHours, int pSalaryPerHour) {
        super(empId, empName);
        this.pNumberOfHours = pNumberOfHours;
        this.pSalaryPerHour = pSalaryPerHour;
    }


}
