package com;


public class FullTimeEmp extends Employee {
    private int fNumberOfHours;
    private int fSalaryPerHour;
    private  int fixedSalary;

    public int getfNumberOfHours() {
        return fNumberOfHours;
    }

    public void setfNumberOfHours(int fNumberOfHours) {
        this.fNumberOfHours = fNumberOfHours;
    }

    public int getfSalaryPerHour() {
        return fSalaryPerHour;
    }

    public void setfSalaryPerHour(int fSalaryPerHour) {
        this.fSalaryPerHour = fSalaryPerHour;
    }

    public int getFixedSalary() {
        return fixedSalary;
    }

    public void setFixedSalary(int fixedSalary) {
        this.fixedSalary = fixedSalary;
    }

    public FullTimeEmp(int empId, String empName, int fNumberOfHours, int fSalaryPerHour, int fixedSalary) {
        super(empId, empName);
        this.fNumberOfHours = fNumberOfHours;
        this.fSalaryPerHour = fSalaryPerHour;
        this.fixedSalary = fixedSalary;
    }




}
