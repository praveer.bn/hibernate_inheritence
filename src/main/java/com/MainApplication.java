package com;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class MainApplication {
    public static void main(String[] args) {
        Configuration configuration=new Configuration().configure("hibernate.cfg.xml");
        SessionFactory sessionFactory=configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();
Transaction transaction = session.beginTransaction();

        Employee employee=new Employee(1,"aman");
        PartTimeEmp partTimeEmp=new PartTimeEmp(2,"sanyami",100,1000);
        FullTimeEmp fullTimeEmp=new FullTimeEmp(3,"ayusi",10,500,50000);
         session.save(employee);
         session.save(partTimeEmp);
         session.save(fullTimeEmp);

         transaction.commit();
         session.close();
         //sessionFactory.close();

    }
}